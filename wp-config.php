<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'consultoriaweb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '61u`9L0u*4B#zx4g9[]/Q!p`%kE62:}k(Dy^?JI6R_ j}St.CmW;Du8y!fN+ 0Kw' );
define( 'SECURE_AUTH_KEY',  '{3yCTo6aE}~ n^Gdj4ZZo?@x7#E7>:@6&_wb.6hH+gOWU+8cVsWjHa;{F4sNM%}9' );
define( 'LOGGED_IN_KEY',    'L#s}bzwM4.8Irdljm3Hd$Ae0$9A:^#?K6D0<dLF{K}x|K8cBuD=(sN+FO EWB7yu' );
define( 'NONCE_KEY',        '{#?Y44w<n`nyzrp4}B;9`a>8WeJpZ|1ER+&B1dm<Z;94q2pSx[qIVRFO/MWibcGP' );
define( 'AUTH_SALT',        '[T,XF6ZqG)84^TZzs$X<faM,iz^1jiAUdHk?x]f*+&99AO7u6.ToC[}=?M8d8-[Y' );
define( 'SECURE_AUTH_SALT', 'sZt~*AsO4A[B9d^R[ 2ji:V~k!!]VdZ}vA;3ox0T|ldn~Lh! * 8]iNUsE_MXt-p' );
define( 'LOGGED_IN_SALT',   'V[;uRM87FEv_ipww,p7;Y6<l+FA =55T^6FU@a`8H] UvK3)3WrFbP$tJw)~f`HA' );
define( 'NONCE_SALT',       'mWQwlfi&}]3]{Hd@o>hjH%@*-0M^~ZP=]Bkt3O)FP#ixctVN.E@LpS2lKH0v&Wwv' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
